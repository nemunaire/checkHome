package api

import (
	"encoding/json"
	"errors"
	"strconv"

	"git.nemunai.re/checkhome/struct"

	"github.com/julienschmidt/httprouter"
)

func init() {
	router.GET("/api/items", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.GetItems()
	}))
	router.DELETE("/api/items", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.ClearItems()
	}))

	router.GET("/api/rooms/:rid/items", apiHandler(roomHandler(func (room ckh.Room, _ []byte) (interface{}, error) {
		if items, err := room.GetItems(); err != nil {
			return nil, err
		} else {
			var res []item
			for _, item := range items {
				if i, err := displayItem(item); err != nil {
					return nil, err
				} else {
					res = append(res, i)
				}
			}
			return res, nil
		}
	})))
	router.POST("/api/rooms/:rid/items", apiHandler(roomHandler(newItem)))
	router.DELETE("/api/rooms/:rid/items", apiHandler(roomHandler(func (room ckh.Room, _ []byte) (interface{}, error) {
		return room.ClearItems()
	})))

	router.GET("/api/rooms/:rid/items/:iid", apiHandler(itemInRoomHandler(func (item ckh.Item, _ ckh.Room, _ []byte) (interface{}, error) {
		return displayItem(item)
	})))
	router.PUT("/api/rooms/:rid/items/:iid", apiHandler(itemInRoomHandler(updateItem)))
	router.DELETE("/api/rooms/:rid/items/:iid", apiHandler(itemInRoomHandler(func (item ckh.Item, _ ckh.Room, _ []byte) (interface{}, error) {
		return item.Delete()
	})))
}

type item struct {
	Id          int64    `json:"id"`
	Label       string   `json:"label"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
}

func displayItem(item ckh.Item) (i item, err error) {
	var tags []ckh.Tag
	if tags, err = item.GetTags(); err != nil {
		return
	} else {
		for _, t := range tags {
			i.Tags = append(i.Tags, t.Label)
		}
	}
	i.Id = item.Id
	i.Label = item.Label
	i.Description = item.Description
	return
}

func itemHandler(f func(ckh.Item, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		if iid, err := strconv.ParseInt(string(ps.ByName("iid")), 10, 64); err != nil {
			return nil, err
		} else if item, err := ckh.GetItem(iid); err != nil {
			return nil, err
		} else {
			return f(item, body)
		}
	}
}

func itemInRoomHandler(f func(ckh.Item, ckh.Room, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		return roomHandler(func (room ckh.Room, _ []byte) (interface{}, error) {
			if iid, err := strconv.ParseInt(string(ps.ByName("iid")), 10, 64); err != nil {
				return nil, err
			} else if item, err := room.GetItem(iid); err != nil {
				return nil, err
			} else {
				return f(item, room, body)
			}
		})(ps, body)
	}
}

func newItem(room ckh.Room, body []byte) (interface{}, error) {
	var ui ckh.Item
	if err := json.Unmarshal(body, &ui); err != nil {
		return nil, err
	}

	if len(ui.Label) == 0 {
		return nil, errors.New("Item's label cannot be empty")
	}

	return ckh.NewItem(ui.Label, ui.Description, room)
}

func updateItem(item ckh.Item, room ckh.Room, body []byte) (interface{}, error) {
	var ui ckh.Item
	if err := json.Unmarshal(body, &ui); err != nil {
		return nil, err
	}

	if len(ui.Label) == 0 {
		return nil, errors.New("Item's label cannot be empty")
	}

	ui.Id = item.Id
	ui.IdRoom = room.Id

	if _, err := ui.Update(); err != nil {
		return nil, err
	} else {
		return ui, nil
	}
}
