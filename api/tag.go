package api

import (
	"encoding/json"
	"errors"
	"strconv"

	"git.nemunai.re/checkhome/struct"

	"github.com/julienschmidt/httprouter"
)

func init() {
	router.GET("/api/tags", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.GetTags()
	}))
	router.POST("/api/tags", apiHandler(newTag))
	router.DELETE("/api/tags", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.ClearTags()
	}))

	router.GET("/api/tags/:tid", apiHandler(tagHandler(func (tag ckh.Tag, _ []byte) (interface{}, error) {
		return tag, nil
	})))
	router.PUT("/api/tags/:tid", apiHandler(tagHandler(updateTag)))
	router.DELETE("/api/tags/:tid", apiHandler(tagHandler(func (tag ckh.Tag, _ []byte) (interface{}, error) {
		if _, err := tag.Delete(); err != nil {
			return nil, err
		} else {
			return map[string]int{}, nil
		}
	})))

	// Item's tags
	router.GET("/api/items/:iid/tags", apiHandler(itemHandler(func (item ckh.Item, _ []byte) (interface{}, error) {
		return item.GetTags()
	})))
	router.DELETE("/api/items/:iid/tags", apiHandler(itemHandler(func (item ckh.Item, _ []byte) (interface{}, error) {
		return item.ClearItemTags()
	})))
	router.PUT("/api/items/:iid/tags/:tid", apiHandler(itemTagHandler(func (item ckh.Item, tag ckh.Tag, _ []byte) (interface{}, error) {
		_, err := tag.AddItem(item)
		return tag, err
	})))
	router.DELETE("/api/items/:iid/tags/:tid", apiHandler(itemTagHandler(func (item ckh.Item, tag ckh.Tag, _ []byte) (interface{}, error) {
		return tag.DeleteItem(item)
	})))
}

func tagHandler(f func(ckh.Tag, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		if tid, err := strconv.ParseInt(string(ps.ByName("tid")), 10, 64); err != nil {
			return nil, err
		} else if tag, err := ckh.GetTag(tid); err != nil {
			return nil, err
		} else {
			return f(tag, body)
		}
	}
}

func itemTagHandler(f func(ckh.Item, ckh.Tag, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		return itemHandler(func (item ckh.Item, _ []byte) (interface{}, error) {
			return tagHandler(func (tag ckh.Tag, _ []byte) (interface{}, error) {
				return f(item, tag, body)
			})(ps, body)
		})(ps, body)
	}
}

func newTag(_ httprouter.Params, body []byte) (interface{}, error) {
	var ut ckh.Tag
	if err := json.Unmarshal(body, &ut); err != nil {
		return nil, err
	}

	if len(ut.Label) == 0 {
		return nil, errors.New("Tag's label cannot be empty")
	}

	return ckh.NewTag(ut.Label)
}

func updateTag(tag ckh.Tag, body []byte) (interface{}, error) {
	var ut ckh.Tag
	if err := json.Unmarshal(body, &ut); err != nil {
		return nil, err
	}

	if len(ut.Label) == 0 {
		return nil, errors.New("Tag's label cannot be empty")
	}

	ut.Id = tag.Id

	if _, err := ut.Update(); err != nil {
		return nil, err
	} else {
		return ut, nil
	}
}
