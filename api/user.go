package api

import (
	"encoding/json"
	"errors"
	"strconv"

	"git.nemunai.re/checkhome/struct"

	"github.com/julienschmidt/httprouter"
)

func init() {
	router.GET("/api/users", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.GetUsers()
	}))
	router.POST("/api/users", apiHandler(newUser))
	router.DELETE("/api/users", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.ClearUsers()
	}))

	router.GET("/api/users/:uid", apiHandler(userHandler(func (user ckh.User, _ []byte) (interface{}, error) {
		return user, nil
	})))
	router.PUT("/api/users/:uid", apiHandler(userHandler(updateUser)))
	router.DELETE("/api/users/:uid", apiHandler(userHandler(func (user ckh.User, _ []byte) (interface{}, error) {
		return user.Delete()
	})))
}

func userHandler(f func(ckh.User, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		if uid, err := strconv.ParseInt(string(ps.ByName("uid")), 10, 64); err != nil {
			return nil, err
		} else if user, err := ckh.GetUser(uid); err != nil {
			return nil, err
		} else {
			return f(user, body)
		}
	}
}

type uploadedUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func newUser(_ httprouter.Params, body []byte) (interface{}, error) {
	var uu uploadedUser
	if err := json.Unmarshal(body, &uu); err != nil {
		return nil, err
	}

	if len(uu.Username) == 0 {
		return nil, errors.New("Username cannot be empty")
	}

	if len(uu.Password) == 0 {
		return nil, errors.New("Password cannot be empty")
	}

	return ckh.NewUser(uu.Username, uu.Password)
}

func updateUser(user ckh.User, body []byte) (interface{}, error) {
	var uu uploadedUser
	if err := json.Unmarshal(body, &uu); err != nil {
		return nil, err
	}

	if len(uu.Username) == 0 {
		return nil, errors.New("Username cannot be empty")
	}

	user.Username = uu.Username

	if len(uu.Password) != 0 {
		user.ChangePassword(uu.Password)
	}

	if _, err := user.Update(); err != nil {
		return nil, err
	} else {
		return user, nil
	}
}
