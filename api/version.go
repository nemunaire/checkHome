package api

import (
	"github.com/julienschmidt/httprouter"
)

func init() {
	router.GET("/api/version", apiHandler(showVersion))
}

func showVersion(_ httprouter.Params, body []byte) (interface{}, error) {
	return map[string]interface{}{"version": 0.1}, nil
}
