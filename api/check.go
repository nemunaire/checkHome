package api

import (
	"encoding/json"
	"errors"
	"strconv"

	"git.nemunai.re/checkhome/struct"

	"github.com/julienschmidt/httprouter"
)

func init() {
	router.GET("/api/checks", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.GetChecks()
	}))
	router.DELETE("/api/checks", apiHandler(func (_ httprouter.Params, _ []byte) (interface{}, error) {
		return ckh.ClearChecks()
	}))

	router.GET("/api/items/:iid/checks", apiHandler(itemHandler(func (item ckh.Item, _ []byte) (interface{}, error) {
		return item.GetChecks()
	})))
	router.POST("/api/items/:iid/checks", apiHandler(itemHandler(newCheck)))

	router.GET("/api/items/:iid/checks/:cid", apiHandler(checkHandler(func (check ckh.Check, _ ckh.Item, _ []byte) (interface{}, error) {
		return check, nil
	})))
	router.PUT("/api/items/:iid/checks/:cid", apiHandler(checkHandler(updateCheck)))
	router.DELETE("/api/items/:iid/checks/:cid", apiHandler(checkHandler(func (check ckh.Check, _ ckh.Item, _ []byte) (interface{}, error) {
		return check.Delete()
	})))

	// in room
	router.GET("/api/rooms/:rid/items/:iid/checks", apiHandler(itemInRoomHandler(func (item ckh.Item, _ ckh.Room, _ []byte) (interface{}, error) {
		return item.GetChecks()
	})))
	router.POST("/api/rooms/:rid/items/:iid/checks", apiHandler(itemInRoomHandler(newCheckInRoom)))

	router.GET("/api/rooms/:rid/items/:iid/checks/:cid", apiHandler(checkInRoomHandler(func (check ckh.Check, _ ckh.Item, _ []byte) (interface{}, error) {
		return check, nil
	})))
	router.PUT("/api/rooms/:rid/items/:iid/checks/:cid", apiHandler(checkInRoomHandler(updateCheck)))
	router.DELETE("/api/rooms/:rid/items/:iid/checks/:cid", apiHandler(checkInRoomHandler(func (check ckh.Check, _ ckh.Item, _ []byte) (interface{}, error) {
		return check.Delete()
	})))
}

func checkHandler(f func(ckh.Check, ckh.Item, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		return itemHandler(func (item ckh.Item, _ []byte) (interface{}, error) {
			if cid, err := strconv.ParseInt(string(ps.ByName("cid")), 10, 64); err != nil {
				return nil, err
			} else if check, err := item.GetCheck(cid); err != nil {
				return nil, err
			} else {
				return f(check, item, body)
			}
		})(ps, body)
	}
}

func checkInRoomHandler(f func(ckh.Check, ckh.Item, []byte) (interface{}, error)) func(httprouter.Params, []byte) (interface{}, error) {
	return func(ps httprouter.Params, body []byte) (interface{}, error) {
		return itemInRoomHandler(func (item ckh.Item, room ckh.Room, _ []byte) (interface{}, error) {
			if cid, err := strconv.ParseInt(string(ps.ByName("cid")), 10, 64); err != nil {
				return nil, err
			} else if check, err := item.GetCheck(cid); err != nil {
				return nil, err
			} else {
				return f(check, item, body)
			}
		})(ps, body)
	}
}

func newCheck(item ckh.Item, body []byte) (interface{}, error) {
	var uc ckh.Check
	if err := json.Unmarshal(body, &uc); err != nil {
		return nil, err
	}

	if len(uc.Passed) == 0 {
		return nil, errors.New("Check's state cannot be empty")
	}

	return item.NewCheck(ckh.User{}, uc.Passed, uc.Comment)
}

func newCheckInRoom(item ckh.Item, _ ckh.Room, body []byte) (interface{}, error) {
	return newCheck(item, body)
}

func updateCheck(check ckh.Check, _ ckh.Item, body []byte) (interface{}, error) {
	var uc ckh.Check
	if err := json.Unmarshal(body, &uc); err != nil {
		return nil, err
	}

	if len(uc.Passed) == 0 {
		return nil, errors.New("Check's state cannot be empty")
	}

	uc.Id = check.Id

	if _, err := uc.Update(); err != nil {
		return nil, err
	} else {
		return uc, nil
	}
}
