package main

import (
	"net/http"
	"path"

	"git.nemunai.re/checkhome/api"

	"github.com/julienschmidt/httprouter"
)

func init() {
	api.Router().GET("/", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, "index.html"))
	})
	api.Router().GET("/items/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, "index.html"))
	})
	api.Router().GET("/rooms/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, "index.html"))
	})
	api.Router().GET("/tags/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, "index.html"))
	})
	api.Router().GET("/users/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, "index.html"))
	})

	api.Router().GET("/css/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, r.URL.Path))
	})
	api.Router().GET("/fonts/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, r.URL.Path))
	})
	api.Router().GET("/img/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, r.URL.Path))
	})
	api.Router().GET("/js/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, r.URL.Path))
	})
	api.Router().GET("/views/*_", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.ServeFile(w, r, path.Join(StaticDir, r.URL.Path))
	})
}
