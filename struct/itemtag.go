package ckh

import ()

type ItemTag struct {
	IdItem int64 `json:"iditem"`
	IdTag  int64 `json:"idtag"`
}

func (i Item) AddTag(t Tag) (ItemTag, error) {
	if _, err := DBExec("INSERT INTO item_tag (id_item, id_tag) VALUES (?, ?)", i.Id, t.Id); err != nil {
		return ItemTag{}, err
	} else {
		return ItemTag{i.Id, t.Id}, nil
	}
}

func (t Tag) AddItem(i Item) (ItemTag, error) {
	if _, err := DBExec("INSERT INTO item_tag (id_item, id_tag) VALUES (?, ?)", i.Id, t.Id); err != nil {
		return ItemTag{}, err
	} else {
		return ItemTag{i.Id, t.Id}, nil
	}
}

func (i Item) DeleteTag(t Tag) (int64, error) {
	if res, err := DBExec("DELETE FROM item_tag WHERE id_item = ? AND id_tag = ?", i.Id, t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (t Tag) DeleteItem(i Item) (int64, error) {
	if res, err := DBExec("DELETE FROM item_tag WHERE id_item = ? AND id_tag = ?", i.Id, t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearItemTags() (int64, error) {
	if res, err := DBExec("DELETE FROM item_tag"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (i Item) GetTags() (tags []Tag, err error) {
	if rows, errr := DBQuery("SELECT T.id_tag, T.label FROM item_tag IT INNER JOIN tags T ON T.id_tag = IT.id_tag WHERE id_item = ?", i.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		tags = make([]Tag, 0)
		for rows.Next() {
			var t Tag
			if err = rows.Scan(&t.Id, &t.Label); err != nil {
				return
			}
			tags = append(tags, t)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (i Item) ClearItemTags() (int64, error) {
	if res, err := DBExec("DELETE FROM item_tag WHERE id_item = ?", i.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (t Tag) ClearItemTags() (int64, error) {
	if res, err := DBExec("DELETE FROM item_tag WHERE id_tag = ?", t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
