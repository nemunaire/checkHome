package ckh

import ()

type Tag struct {
	Id    int64  `json:"id"`
	Label string `json:"label"`
}

func GetTags() (tags []Tag, err error) {
	if rows, errr := DBQuery("SELECT id_tag, label FROM tags"); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		tags = make([]Tag, 0)
		for rows.Next() {
			var t Tag
			if err = rows.Scan(&t.Id, &t.Label); err != nil {
				return
			}
			tags = append(tags, t)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetTag(id int64) (t Tag, err error) {
	err = DBQueryRow("SELECT id_tag, label FROM tags WHERE id_tag = ?", id).Scan(&t.Id, &t.Label)
	return
}

func NewTag(label string) (Tag, error) {
	if res, err := DBExec("INSERT INTO tags (label) VALUES (?)", label); err != nil {
		return Tag{}, err
	} else if tid, err := res.LastInsertId(); err != nil {
		return Tag{}, err
	} else {
		return Tag{tid, label}, nil
	}
}

func (t Tag) Update() (int64, error) {
	if res, err := DBExec("UPDATE tags SET label = ? WHERE id_tag = ?", t.Label, t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (t Tag) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM tags WHERE id_tag = ?", t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearTags() (int64, error) {
	if res, err := DBExec("DELETE FROM tags"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
