package ckh

import (
	"crypto"
	_ "crypto/sha1"
)

type User struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
	password []byte
}

func GetUsers() (users []User, err error) {
	if rows, errr := DBQuery("SELECT id_user, username, password FROM users"); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		users = make([]User, 0)
		for rows.Next() {
			var u User
			if err = rows.Scan(&u.Id, &u.Username, &u.password); err != nil {
				return
			}
			users = append(users, u)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetUser(id int64) (u User, err error) {
	err = DBQueryRow("SELECT id_user, username, password FROM users WHERE id_user = ?", id).Scan(&u.Id, &u.Username, &u.password)
	return
}

func hashPassword(password string) []byte {
	hash := crypto.SHA1.New()
	return hash.Sum([]byte(password))
}

func NewUser(username string, password string) (User, error) {
	hash := hashPassword(password)

	if res, err := DBExec("INSERT INTO users (username, password) VALUES (?, ?)", username, hash); err != nil {
		return User{}, err
	} else if uid, err := res.LastInsertId(); err != nil {
		return User{}, err
	} else {
		return User{uid, username, hash}, nil
	}
}

func (u User) checkPassword(password string) bool {
	givenHash := hashPassword(password)

	if len(givenHash) != len(u.password) {
		return false
	}

	for k := range u.password {
		if u.password[k] != givenHash[k] {
			return false
		}
	}

	return true
}

func (u User) ChangePassword(password string) {
	u.password = hashPassword(password)
}

func (u User) Update() (int64, error) {
	if res, err := DBExec("UPDATE users SET username = ?, password = ? WHERE id_user = ?", u.Username, u.password, u.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (u User) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM users WHERE id_user = ?", u.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearUsers() (int64, error) {
	if res, err := DBExec("DELETE FROM users"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
