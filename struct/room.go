package ckh

import ()

type Room struct {
	Id    int64  `json:"id"`
	Label string `json:"label"`
}

func GetRooms() (rooms []Room, err error) {
	if rows, errr := DBQuery("SELECT id_room, label FROM rooms"); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			var r Room
			if err = rows.Scan(&r.Id, &r.Label); err != nil {
				return
			}
			rooms = append(rooms, r)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetRoom(id int64) (r Room, err error) {
	err = DBQueryRow("SELECT id_room, label FROM rooms WHERE id_room = ?", id).Scan(&r.Id, &r.Label)
	return
}

func NewRoom(label string) (Room, error) {
	if res, err := DBExec("INSERT INTO rooms (label) VALUES (?)", label); err != nil {
		return Room{}, err
	} else if rid, err := res.LastInsertId(); err != nil {
		return Room{}, err
	} else {
		return Room{rid, label}, nil
	}
}

func (r Room) Update() (int64, error) {
	if res, err := DBExec("UPDATE rooms SET label = ? WHERE id_room = ?", r.Label, r.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (r Room) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM rooms WHERE id_room = ?", r.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearRooms() (int64, error) {
	if res, err := DBExec("DELETE FROM rooms"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
