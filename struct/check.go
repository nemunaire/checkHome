package ckh

import (
	"time"
)

type Check struct {
	Id       int64     `json:"id"`
	IdItem   int64     `json:"id_item"`
	IdUser   int64     `json:"id_user"`
	Date     time.Time `json:"date"`
	Passed   string    `json:"passed"`
	Comment  string    `json:"comment",omitempty`
}

func GetChecks() (checks []Check, err error) {
	if rows, errr := DBQuery("SELECT id_check, id_item, id_user, date, passed, comment FROM checks"); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		checks = make([]Check, 0)
		for rows.Next() {
			var c Check
			if err = rows.Scan(&c.Id, &c.IdItem, &c.IdUser, &c.Date, &c.Passed, &c.Comment); err != nil {
				return
			}
			checks = append(checks, c)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (i Item) GetChecks() (checks []Check, err error) {
	if rows, errr := DBQuery("SELECT id_check, id_item, id_user, date, passed, comment FROM checks WHERE id_item = ?", i.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		checks = make([]Check, 0)
		for rows.Next() {
			var c Check
			if err = rows.Scan(&c.Id, &c.IdItem, &c.IdUser, &c.Date, &c.Passed, &c.Comment); err != nil {
				return
			}
			checks = append(checks, c)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetCheck(id int64) (c Check, err error) {
	err = DBQueryRow("SELECT id_check, id_item, id_user, date, passed, comment FROM checks WHERE id_check = ?", id).Scan(&c.Id, &c.IdItem, &c.IdUser, &c.Date, &c.Passed, &c.Comment)
	return
}

func (i Item) GetCheck(id int64) (c Check, err error) {
	err = DBQueryRow("SELECT id_check, id_item, id_user, date, passed, comment FROM checks WHERE id_check = ? AND id_item = ?", id, i.Id).Scan(&c.Id, &c.IdItem, &c.IdUser, &c.Date, &c.Passed, &c.Comment)
	return
}

func (i Item) NewCheck(user User, state string, comment string) (Check, error) {
	if res, err := DBExec("INSERT INTO checks (id_item, id_user, date, passed, comment) VALUES (?, ?, ?, ?, ?)", i.Id, user.Id, time.Now(), state, comment); err != nil {
		return Check{}, err
	} else if cid, err := res.LastInsertId(); err != nil {
		return Check{}, err
	} else {
		return Check{cid, i.Id, user.Id, time.Now(), state, comment}, nil
	}
}

func (c Check) Update() (int64, error) {
	if res, err := DBExec("UPDATE checks SET id_item = ?, id_user = ?, date = ?, passed = ?, comment = ? WHERE id_check = ?", c.IdItem, c.IdUser, c.Date, c.Passed, c.Comment, c.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (c Check) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM checks WHERE id_check = ?", c.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearChecks() (int64, error) {
	if res, err := DBExec("DELETE FROM checks"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
