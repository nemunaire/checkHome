package ckh

import ()

type Item struct {
	Id          int64  `json:"id"`
	Label       string `json:"label"`
	Description string `json:"description"`
	IdRoom      int64  `json:"id_room"`
}

func GetItems() (items []Item, err error) {
	if rows, errr := DBQuery("SELECT id_item, label, description, id_room FROM items"); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		items = make([]Item, 0)
		for rows.Next() {
			var i Item
			if err = rows.Scan(&i.Id, &i.Label, &i.Description, &i.IdRoom); err != nil {
				return
			}
			items = append(items, i)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetItem(id int64) (i Item, err error) {
	err = DBQueryRow("SELECT id_item, label, description, id_room FROM items WHERE id_item = ?", id).Scan(&i.Id, &i.Label, &i.Description, &i.IdRoom)
	return
}

func (r Room) GetItems() (items []Item, err error) {
	if rows, errr := DBQuery("SELECT id_item, label, description, id_room FROM items WHERE id_room = ?", r.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		items = make([]Item, 0)
		for rows.Next() {
			var i Item
			if err = rows.Scan(&i.Id, &i.Label, &i.Description, &i.IdRoom); err != nil {
				return
			}
			items = append(items, i)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func (r Room) GetItem(id int64) (i Item, err error) {
	err = DBQueryRow("SELECT id_item, label, description, id_room FROM items WHERE id_room = ? AND id_item = ?", r.Id, id).Scan(&i.Id, &i.Label, &i.Description, &i.IdRoom)
	return
}

func NewItem(label string, description string, room Room) (Item, error) {
	if res, err := DBExec("INSERT INTO items (label, description, id_room) VALUES (?, ?, ?)", label, description, room.Id); err != nil {
		return Item{}, err
	} else if iid, err := res.LastInsertId(); err != nil {
		return Item{}, err
	} else {
		return Item{iid, label, description, room.Id}, nil
	}
}

func (i Item) Update() (int64, error) {
	if res, err := DBExec("UPDATE items SET label = ?, description = ?, id_room = ? WHERE id_item = ?", i.Label, i.Description, i.IdRoom, i.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (i Item) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM items WHERE id_item = ?", i.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearItems() (int64, error) {
	if res, err := DBExec("DELETE FROM items"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (r Room) ClearItems() (int64, error) {
	if res, err := DBExec("DELETE FROM items WHERE id_room = ?", r.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
