package ckh

import (
	"database/sql"
	"log"
	"os"
	"strings"
	"time"
	_ "github.com/go-sql-driver/mysql"
)

// db stores the connection to the database
var db *sql.DB

// DSNGenerator returns DSN filed with values from environment
func DSNGenerator() string {
	db_user := "checkhome"
	db_password := "checkhome"
	db_host := ""
	db_db := "checkhome"

	if v, exists := os.LookupEnv("MYSQL_HOST"); exists {
		db_host = v
	}
	if v, exists := os.LookupEnv("MYSQL_PASSWORD"); exists {
		db_password = v
	} else if v, exists := os.LookupEnv("MYSQL_ROOT_PASSWORD"); exists {
		db_user = "root"
		db_password = v
	}
	if v, exists := os.LookupEnv("MYSQL_USER"); exists {
		db_user = v
	}
	if v, exists := os.LookupEnv("MYSQL_DATABASE"); exists {
		db_db = v
	}

	return db_user + ":" + db_password + "@" + db_host + "/" + db_db
}

// DBInit establishes the connection to the database
func DBInit(dsn string) (err error) {
	if db, err = sql.Open("mysql", dsn + "?parseTime=true&foreign_key_checks=1"); err != nil {
		return
	}

	_, err = db.Exec(`SET SESSION sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';`)
	for i := 0; err != nil && i < 15; i += 1 {
		if _, err = db.Exec(`SET SESSION sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';`); err != nil && i <= 5 {
			log.Println("An error occurs when trying to connect to DB, will retry in 2 seconds: ", err)
			time.Sleep(2 * time.Second)
		}
	}

	return
}

// DBCreate creates all necessary tables used by the package
func DBCreate() (err error) {
	ct := `
CREATE TABLE IF NOT EXISTS rooms (id_room INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, label TEXT NOT NULL);
CREATE TABLE IF NOT EXISTS tags (id_tag INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, label TEXT NOT NULL);
CREATE TABLE IF NOT EXISTS items (id_item INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, label TEXT NOT NULL, description TEXT NOT NULL, id_room INTEGER);
CREATE TABLE IF NOT EXISTS item_tag (id_item INTEGER NOT NULL, id_tag INTEGER NOT NULL);
CREATE TABLE IF NOT EXISTS users (id_user INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, username VARCHAR(255), password BINARY(64));
CREATE TABLE IF NOT EXISTS checks (id_check INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, id_item INTEGER NOT NULL, id_user INTEGER NOT NULL, date DATETIME DEFAULT CURRENT_TIMESTAMP, passed ENUM('yes', 'no', 'yesbut', 'nobut', 'next') NOT NULL, comment TEXT NOT NULL);
`
	for _, ln := range strings.Split(ct, "\n") {
		if len(ln) == 0 {
			continue
		}
		if _, err = db.Exec(ln); err != nil {
			return
		}
	}
	return
}

func DBClose() error {
	return db.Close()
}

func DBPrepare(query string) (*sql.Stmt, error) {
	return db.Prepare(query)
}

func DBQuery(query string, args ...interface{}) (*sql.Rows, error) {
	return db.Query(query, args...)
}

func DBExec(query string, args ...interface{}) (sql.Result, error) {
	return db.Exec(query, args...)
}

func DBQueryRow(query string, args ...interface{}) *sql.Row {
	return db.QueryRow(query, args...)
}
