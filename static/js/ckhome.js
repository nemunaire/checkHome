angular.module("CheckHomeApp", ["ngRoute", "ngResource", "ngSanitize"])
    .config(function($routeProvider, $locationProvider) {
	$routeProvider
	    .when("/items", {
		templateUrl: "views/items.html",
		controller: "RoomsController"
	    })
	    .when("/rooms", {
		templateUrl: "views/rooms.html",
		controller: "RoomsController"
	    })
	    .when("/tags", {
		templateUrl: "views/tags.html",
		controller: "TagsController"
	    })
	    .when("/users", {
		templateUrl: "views/users.html",
		controller: "UsersController"
	    })
	    .when("/", {
		templateUrl: "views/home.html",
		controller: "RoomsController"
	    });
	$locationProvider.html5Mode(true);
    });

angular.module("CheckHomeApp")
    .factory("Version", function($resource) {
	return $resource("/api/version")
    })
    .factory("Room", function($resource) {
	return $resource("/api/rooms/:roomId", { roomId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("ItemChecks", function($resource) {
	return $resource("/api/items/:itemId/checks/:checkId", { itemId: '@id', checkId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("ItemRoom", function($resource) {
	return $resource("/api/rooms/:roomId/items/:itemId", { roomId: '@id', itemId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("User", function($resource) {
	return $resource("/api/users/:userId", { userId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("TagsItem", function($resource) {
	return $resource("/api/items/:itemId/tags/:tagId", { itemId: '@id', tagId: '@id' }, {
	    'save': {method: 'PUT'},
	    'update': {method: 'PUT'},
	})
    })
    .factory("Tag", function($resource) {
	return $resource("/api/tags/:tagId", { tagId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    });

function newCheck(ItemChecks, iid, passed, cb) {
    var c = new ItemChecks();
    c.passed = passed;
    c.comment = $("#comment").val();
    c.$save({itemId: iid}, function(res) {
	$("#comment").val("");
	$('#commentModal').modal("hide");
	cb(res);
    });
}

angular.module("CheckHomeApp")
    .controller("VersionController", function($scope, Version) {
	$scope.v = Version.get();
    })
    .controller("RoomsController", function(ItemChecks, Room, $scope, $rootScope) {
	$scope.rooms = Room.query();
	$rootScope.tagsX = [];
	$rootScope.tagsReverse = false;
	$rootScope.tagsShown = {};
	$rootScope.stateShown = { "yes": true, "yesbut": true, "nobut": true, "no": true, "next": false , "na": true };
        $rootScope.allDatesShown = false;

	$scope.newRoom = function() {
	    var t = new Room();
	    t.edit = true;
	    $scope.rooms.push(t);
	}
	$scope.toggleRoom = function() {
	    this.room.closed = !this.room.closed;
	}
	$scope.showOnlyThisRoom = function() {
	    $scope.rooms.forEach(function(r) {
		r.closed = true;
	    });
	    this.room.closed = false;
	}
	$scope.toggleDate = function(state) {
	    $rootScope.allDatesShown = !$rootScope.allDatesShown;
	}
	$scope.toggleStateS = function(state) {
	    $rootScope.stateShown[state] = !$rootScope.stateShown[state];
	}
	$scope.toggleAllStates = function() {
	    Object.keys($rootScope.stateShown).forEach(function(v) {
		$rootScope.stateShown[v] = $rootScope.statesSelect;
	    })
	    $rootScope.statesSelect = !$rootScope.statesSelect;
	}
	$scope.editRoom = function() {
	    this.room.edit = true;
	}
	$scope.saveRoom = function() {
	    if (this.room.id === undefined)
		this.room.$save()
	    else
		this.room.$update();
	}
	$scope.delRoom = function(rk) {
	    this.room.$delte().then(function() {
		$scope.room.splice(rk, 1);
	    });
	}

	$scope.skipCheck = function() {
	    $rootScope.selectedItem.action = "next";
	    $scope.submitModal();
	}
	$scope.submitModal = function() {
	    newCheck(ItemChecks, $rootScope.selectedItem.id, $rootScope.selectedItem.action, $rootScope.selectedItem.cb)
	}
    })
    .controller("ItemsRoomController", function($scope, $rootScope, ItemRoom) {
	$scope.items = ItemRoom.query({roomId: $scope.room.id});

	$scope.items.$promise.then(function(){
	    $scope.items.forEach(function(item) {
		if (item.tags != null)
		    item.tags.forEach(function(v) {
			if ($rootScope.tagsX.indexOf(v) == -1) {
			    $rootScope.tagsX.push(v);
			    $rootScope.tagsShown[v] = true;
			}
		})
	    })
	});

	$scope.filterByTag = function() {
	    if (this.item.tags == null)
		return true;

	    if (!$rootScope.tagsReverse) {
		var display = false;
		this.item.tags.forEach(function(v) {
		    $rootScope.tagsX.forEach(function(t) {
			if (display || ($rootScope.tagsShown[t] && t == v))
			    display = true;
		    });
		});
		return display;
	    } else {
		var display = true;
		this.item.tags.forEach(function(v) {
		    $rootScope.tagsX.forEach(function(t) {
			if (!display || ($rootScope.tagsShown[t] && t == v))
			    display = false;
		    });
		});
		return display;
	    }
	}

	$scope.newItem = function() {
	    var t = new ItemRoom();
	    t.edit = true;
	    $scope.items.push(t);
	}
	$scope.toggleDescription = function() {
	    this.item.open = !this.item.open;
	}
	$scope.editItem = function() {
	    this.item.edit = true;
	}
	$scope.saveItem = function() {
	    if (this.item.id === undefined)
		this.item.$save({roomId: this.room.id})
	    else
		this.item.$update({roomId: this.room.id});
	}
	$scope.delItem = function(ik) {
	    this.item.$delete({roomId: this.room.id}).then(function() {
		$scope.items.splice(ik, 1);
	    });
	}
    })
    .controller("ChecksItemController", function($scope, ItemChecks, $rootScope) {
	$rootScope.selectedItem = {};
	$scope.ncomment = "";
	$scope.checks = ItemChecks.query({itemId: $scope.item.id});

	$scope.min_checks = function() {
	    function state2int(state) {
		switch(state) {
		case "yes":
		    return 4;
		case "yesbut":
		    return 3;
		case "nobut":
		    return 2;
		case "no":
		    return 1;
		case "next":
		    return 5;
		default:
		    return 6;
		}
	    }
	    var min = "N/A";
	    angular.forEach($scope.checks, function(check) {
		if (($rootScope.allDatesShown || new Date(check["date"]).getTime() > Date.now() - 86400000) && state2int(min) > state2int(check["passed"]))
		    min = check["passed"];
	    });
	    return min;
	}
	$scope.filterByCheck = function() {
	    if ((this.checks == null || this.checks.length == 0) && $rootScope.stateShown["na"])
		return true;

	    var display = false;
	    this.checks.forEach(function(v) {
		Object.keys($rootScope.stateShown).forEach(function(k) {
		    if(display || (k == v["passed"] && $rootScope.stateShown[k]))
			display = true;
		});
	    });
	    return display;
	}

	$scope.checkCommon = function(modal, res) {
	    if (modal) {
		var cb = function(res) { $scope.checks.push(res); }
		$rootScope.selectedItem = this.item;
		$rootScope.selectedItem.action = res;
		$rootScope.selectedItem.cb = cb;
		$('#commentModal').modal("show");
		document.getElementById("comment").focus();
	    } else {
		newCheck(ItemChecks, $scope.item.id, res, function(res) { $scope.checks.push(res); });
	    }
	}
	$scope.checkOk = function(modal) {
	    $scope.checkCommon(modal, "yes");
	}
	$scope.checkMok = function(modal) {
	    $scope.checkCommon(modal, "yesbut");
	}
	$scope.checkMko = function(modal) {
	    $scope.checkCommon(modal, "nobut");
	}
	$scope.checkKo = function(modal) {
	    $scope.checkCommon(modal, "no");
	}

	$scope.delCheck = function(ck) {
	    this.check.$delete({itemId: $scope.item.id}).then(function() {
		$scope.checks.splice(ck, 1);
	    });
	}
    })
    .controller("TagsController", function($scope, $rootScope, Tag) {
	$scope.tags = Tag.query();

	$rootScope.tagsSelect = false;
	$scope.toggleAllTags = function() {
	    $rootScope.tagsX.forEach(function(v) {
		$rootScope.tagsShown[v] = $rootScope.tagsSelect;
	    })
	    $rootScope.tagsSelect = !$rootScope.tagsSelect;
	}
	$scope.toggleReverseTags = function(tag) {
	    $rootScope.tagsReverse = !$rootScope.tagsReverse;
	}
	$scope.toggleTagS = function(tag) {
	    $rootScope.tagsShown[tag] = !$rootScope.tagsShown[tag];
	    $rootScope.tagsSelect = false;
	}

	$scope.newTag = function() {
	    var t = new Tag();
	    t.edit = true;
	    $scope.tags.push(t);
	}
	$scope.editTag = function() {
	    this.tag.edit = true;
	}
	$scope.saveTag = function() {
	    if (this.tag.id === undefined)
		this.tag.$save()
	    else
		this.tag.$update();
	}
	$scope.delTag = function(tk) {
	    this.tag.$delete().then(function() {
		$scope.tags.splice(tk, 1);
	    });
	}
    })
    .controller("TagsItemController", function($scope, TagsItem, $http) {
	$scope.itags = TagsItem.query({itemId: $scope.item.id});

	$scope.newItemTag = function() {
	    var t = new TagsItem();
	    t.edit = true;
	    $scope.itags.push(t);
	}
	$scope.editItemTag = function() {
	    this.tag.val = this.tag.id;
	    this.tag.edit = true;
	}
	$scope.saveItemTag = function() {
	    if (this.tag.id === undefined)
		this.tag.$save({itemId: $scope.item.id, tagId: this.tag.val})
	    else {
		var v = this.tag;
		var val = this.tag.val;
		v.$delete({itemId: $scope.item.id, tagId: v.id}).then(function() {
		    v.$save({itemId: $scope.item.id, tagId: val});
		});
	    }
	}
	$scope.delItemTag = function(tk) {
	    this.tag.$delete({itemId: $scope.item.id, tagId: this.tag.id}).then(function() {
		$scope.itags.splice(tk, 1);
	    });
	}
    })
    .controller("UsersController", function($scope, User, $http, $location) {
	$scope.users = User.query();

	$scope.newUser = function() {
	    var t = new User();
	    t.edit = true;
	    t.editpass = true;
	    $scope.users.push(t);
	}
	$scope.editUser = function() {
	    this.user.edit = true;
	}
	$scope.editUserPass = function() {
	    this.user.editpass = true;
	}
	$scope.saveUser = function() {
	    if (this.user.id === undefined)
		this.user.$save()
	    else
		this.user.$update();
	}
	$scope.delUser = function(uk) {
	    this.user.$delete().then(function(){
		$scope.users.splice(uk, 1);
	    });
	}
	$scope.razchecks = function() {
	    if (confirm("Sûr d'effacer tous les tests effectués ?"))
		$http.delete("/api/checks").then(function() {
		    $location.url("/");
		});
	}
    });
